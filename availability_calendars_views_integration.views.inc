<?php


function availability_calendars_views_integration_views_data() {
	
	
	$data['field_data_field_calendar']['table']['base'] = array(
		'field' => 'cid',
		'title' => t('Calendar Date Availability Table'),
		'help' => t('An listing of dates per calendar and status.'),
		'weight' => -10,
	);

	$data['field_data_field_calendar']['table']['group'] = t('Availability Calendars');
	$data['availability_calendar_availability']['table']['group'] = t('Availability Calendars');
	
	$data['field_data_field_calendar']['table']['join'] = array(
    // this explains how the 'availability_calendar_availability' table (named in the line above)
    // links toward the node_revision table.
		'availability_calendar_availability' => array(
		  'handler' => 'views_join', // this is actually optional
		  'left_table' => 'availability_calendar_availability', // Because this is a direct link it could be left out.
		  'left_field' => 'cid',
		  'field' => 'field_calendar_cid',
		 ),
		'node' => array(
		  'handler' => 'views_join', // this is actually optional
		  'left_table' => 'node', // Because this is a direct link it could be left out.
		  'left_field' => 'nid',
		  'field' => 'entity_id',

		 ),		 

	);

	
	$data['availability_calendar_availability']['date'] = array(
		'title' => t('Availability Date'), 
		'help' => t('The Availability Date.'), 
		   'field' => array(
			  'handler' => 'views_handler_field_datetime', 
			  'click sortable' => TRUE, // This is use by the table display plugin.
			), 
			'sort' => array(
			  'handler' => 'views_handler_sort_datetime',
			), 
			'filter' => array(
			  'handler' => 'views_handler_filter_datetime',
			), 
			'argument' => array(
			  'handler' => 'views_handler_argument_datetime',
			),		
	);

	$data['availability_calendar_availability']['sid'] = array(
		'title' => t('Availability Status ID'), 
		'help' => t('The Availability Status ID.'), 
		'field' => array(
			'handler' => 'views_handler_field_numeric', 
			'click sortable' => TRUE,
		), 
		'sort' => array(
			'handler' => 'views_handler_sort_numeric',
		), 
		'filter' => array(
			'handler' => 'views_handler_filter_numeric',
		), 
		'argument' => array(
			'handler' => 'views_handler_argument_numeric',
		),
	);	

	$data['field_data_field_calendar']['entity_id'] = array(
		'title' => t('Node ID'), 
		'help' => t('Node ID.'), 
		'relationship' => array(
		   'base' => 'node',
		   'field' => 'nid',
		   'handler' => 'views_handler_relationship',
		   'label' => t('Node'),
		),
		'field' => array(
			'handler' => 'views_handler_field_node', 
			'click sortable' => TRUE,
		), 	
				'sort' => array(
			'handler' => 'views_handler_sort',
		), 
		'filter' => array(
			'handler' => 'views_handler_filter_numeric',
		), 
		'argument' => array(
			'handler' => 'views_handler_argument_node_nid',
			'name field' => 'title', // the field to display in the summary.
			'numeric' => TRUE,
			'validate type' => 'nid',
		  ),
	);	

	
	
	return $data;
}



